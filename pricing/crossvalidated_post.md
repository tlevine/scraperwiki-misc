Events occurred on certain datetimes. I want to bin these by days so that I can explain patterns easily.

Before I bin them, they look like this.

    df <- data.frame(
      datetime=as.POSIXlt(c(
        "2011-05-04 17:24:08", "2011-05-04 17:52:51",
        "2011-05-06 04:45:02", "2011-05-06 12:18:04"
      ))
    )
    df$date <- as.Date(df$datetime)

Once I bin them, I have a dataset that looks like this. (I'm actually doing more than counts, but counts should be enough to explain this.)

    ddply(df,'date',nrow)
          date V1
    2011-05-04  2
    2011-05-06  2

I have a row for each day that occurred, but I have no rows for days where the event did not occur. I want it to look more like this.

          date V1
    2011-05-04  2
    2011-05-05  0
    2011-05-06  2


Said in words, I want to add a row for each date that isn't already in the date column. There must be some elegant, standard way of doing this.

I could see this being done before/during the binning or after the binning.
