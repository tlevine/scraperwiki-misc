Scraping libraries Tom wants to make

* Dblist: A list-like object backed by a file or database
* Highwall: A relaxing interface to SQLite
* Bucket-Wheel: A scraping framework that abstracts everything except parsing
* Dump Truck: A collection of scraping helpers
