\documentclass{article}
\usepackage{soul}

\title{How much work is monitoring?}
\author{Thomas Levine}

\begin{document}
\maketitle
\abstract{A customer may request that her scrapers be monitored,
and we may want to provide a monitoring service by which accept
a fixed monthly charge in return for agreeing to fix her scrapers
in a timely fashion should they break for reasonable reasons.
This document proposes limits on the problems that such a plan
should cover and proposes a method for estimating how much time
we need in order to provide a monitoring service.}

First, I propose a plan that allows us to make a particular
sort of monitoring agreement. Next, I propose a method of
determining the cost of the monitoring.

\section{The agreement}
In exchange for a monthly fee, we will monitor the scraper.
We discover problems by two means:
\begin{enumerate}
\item Scraper exceptions
\item Communication from the customer (Some problems won't raise exceptions.)
\end{enumerate}
Call either such notification an ``error discovery''. We guarantee to
fix the problem within a particular timespan for the overwhelming majority
of error discoveries. For example, we might guarantee that problems
be fixed within three days in 99.7\% of error discoveries. For our end,
we could phrase this proportion somewhat differently. 99.7\% is about
\(1-\frac{1}{365\frac{1}{4}}\), so we expect to have one day every year
during which we will have failed to respond to the errors that we are
due to respond to that day.

Within a particular number of days, we will investigate the problem.
If the problem is caused by anything \emph{other than} a change in
the data delivery process or the data format, we will also fix the problem
within these days. If the problem's cause is among these two, we
will instead provide a report about the problem within the few days.%
\footnote{A change in the data delivery or format could render the previous
scraper entirely useless, so we might have to write a whole new scraper
in order to fix such a problem.}

\section{How much work does such a plan require on our part?}
First, calculate how much time we need someone on-call in order
to be sure that we will will respond to the errors in a timely
fashion every time. This involves making some quite invalidating
assumptions; assume the following.
\begin{itemize}
\item All of the scrapers that we are monitoring break at the same
rate of once every \(\lambda\) months.
\item Each of the scrapers takes \(t\) hours to fix.
\end{itemize}

Since the scrapers break once every \(\lambda\) months,
we can also say that there is a \( p = \frac{1}{\lambda} \times \frac{1 month}{20 business days} \)
chance that a particular scraper will break on a particular day.

Per our agreements, we know that
\begin{itemize}
\item We guarantee to fix each scraper within the same number of days, \(d\).
\item We are monitoring \(n\) scrapers.
\item We expect \(u\) uptime. \(u\) might be \(99.7\%\), or one problem day per year.
\end{itemize}

Given the following parameters, we need developer(s) on call for
this many% 
\footnote{This formula is actually somewhat wrong because it doesn't sufficiently
consider the dependence and independence between days; more on that later.}
hours per day.

\[
  \frac{t}{d}
  \times
  \left(
    NormalCDF^{-1} \left( u \right) \times
    \frac{p \left(1 - p\right)}{n} + np
  \right)
\]

I haven't tested this empirically, but
I derived this on paper, and I'll add the derivation eventually.

But here is a simplified explanation for now. The number of errors
that arises per day is distributed binomially. I approximated it
with a normal distribution and could thus determine how unlikely
it would be for us to see a particularly high (one-sided test)
value given the assumed chance of error.

Similarly, I could find a particular number of errors in one day for a particular likelihood.
I assigned the uptime \(u\) to this likelihood and arrived at an
adjustment (between zero and one) for the time allocated to monitoring.
Call this adjustment \(a\)

Multiplying this adjustment by the maximum time needed to be sure
of being able to fix the problems gives us the time we need to allocate
in order to fix the problems \(u\) of the time. So

\[ u = a \times \frac{nt}{d} \]

Factoring this led to the equation further above.

\subsection{Variables}
Said less mathematically, this model above uses these five variables.
\begin{enumerate}
\item The speed with which we say we will fix scrapers
\item The rate with which we fix scrapers within this timeframe
\item The rate with which scrapers break
\item How much time Tom should keep available in case of scraper errors
\item The variety of scrapers that we are monitoring
\end{enumerate}

Four of those is enough to determine the fifth.

\subsection{Limitations}
%Aidan
The one thing the model does not take account of is that a badly written scraper
will generate more errors than a well written scraper.
This concern concerns is aggregated into
\begin{itemize}
\item The rate with which we fix scrapers within this timeframe
\item The rate with which scrapers break
\end{itemize}
Moreover, the current model assumes that all scrapers are the same.
That might not be a problem in practice.
And it won't be hard to adjust the model to account more finely for variation in scrapers.

Also, the model stops at computing the necessary time and thus
doesn't really account for the cost of splitting a data scientist's time.



\section{Can we charge independently of scraper complexity?}
Tom would delight in the simplicity of being able to charge a rate
that does not depend on scraper quality.

Implementing this is reasonably straightforword; rather than basing the
rate on scraper quality, we would expect that we build scrapers
of a particular quality such that they only break with a particular rate.
This would determine one of the five variables.

\begin{enumerate}
\item The speed with which we say we will fix scrapers
\item The rate with which we fix scrapers within this timeframe
\item \st{The rate with which scrapers break}
\item How much time Tom should keep available in case of scraper errors
\item The variety of scrapers that we are monitoring
\end{enumerate}

Our agreements will tell us these three variables, and plugging them into
the model gives us a necessary on-call time. We could multiply that on-call
time by some pricing factor (i.e., hourly rate) in order to list monthly
rates for different service levels.

\subsection{An example}
Let's say that we expect that scrapers that we write or subcontract be of
such quality that they break no less than once per month. To quantify this,
we assume \(\lambda \ge 1 month\). We convert this to a per--business-day
probability of \(\frac{1}{20}\) and then plug it into the available-time
expression. Let's also expect that our scrapers take 8 hours to fix and
set an uptime of 99.7\%.

\[
  t_{available} = \frac{8}{d}
  \times
  \left(
    NormalCDF^{-1} \left( 0.997 \right) \times
    \frac{19}{400\times n} + \frac{1}{20} \times n
  \right)
\]

If we increase $n$, the equation converges pretty quickly to
\(t_{available} = \frac{8}{d}\). This makes the costs rather
straightforward. Let's say that eight hours of the data scientist's
time on call costs \(\$25 \times 8 = \$400\). So

\begin{tabular}{ r r }
\hline
Time until recovery & Cost per month \\
\hline
1 business day & \$4000 \\
2 business days & \$2000 \\
1 week & \$800 \\
\hline
\end{tabular}

\end{document}
