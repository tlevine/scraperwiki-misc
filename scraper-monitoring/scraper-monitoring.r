available <- function(time_to_fix = 8, days_to_fix = 2, uptime = 0.997, p = 1/80, n = 1){
 (time_to_fix/days_to_fix) * (
   qnorm( uptime ) * p * (1 - p) / n + n*p
 )
}

agreement_decision <- function(days_to_fix, n = 100){
  # Hours per month for one scraper
  20 * available(days_to_fix = days_to_fix, n = n) / n
}
