After SXSW, I could go to the [Austin Python Meetup](http://www.meetup.com/austinpython/)
on March 14.

I didn't find many events in Austin after SXSW, but here
are some groups that might add meetups soon.

* [Quantified Self Austin](http://www.meetup.com/Quantified-Self-Austin/)
* [RUG](http://www.meetup.com/Austin-R-User-Group/)

Leaving Union Station in DC, I ran into a friend who lives
in Houston and encouraged me to crash with her for the week
between SXSW and my talk for Cindy Royal's class. Here are
some timely Houston events.

* [TX/RX Labs weekly meetup](http://www.meetup.com/TXRXLabs/events/52218012/)
* [PyHou](http://www.meetup.com/python-14/events/54177062/)

I found [nothing relevant](http://search.txstate.edu/search?q=colloquium&site=txstate_no_users&client=txstate&output=xml_no_dtd&proxystylesheet=txstate&sitesearch=&x=0&y=0)
[at Texas State University](http://events.txstate.edu/cal/main/showMain.rdo)
at the times that I would be there,
but Cindy offered to give me a tour and introduce me to some people
during the day on March 21.

There's the [Austin Open Source Meetup Group](http://www.meetup.com/opensource-70/),
which has a meeting on Mar 22.

I didn't find much on Meetup.com in DC for the week before
the data camp, but I suspect that more things will be added
there in the coming weeks.
