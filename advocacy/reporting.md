If I'm going to be going to loads of developery events in order to
tell people about ScraperWiki, I'm going to want to report some
relevant information on each. For each one, I might record

* Name, date, location
* The number of people there
* Name and email address for anyone whose contact information I collect
