\documentclass{article}
\usepackage{graphicx}

\title{Why so many scrapers?}
\author{Thomas Levine}

\begin{document}
\maketitle

<<label=load,echo=FALSE>>=
source('chainsaw.R')
@


\section{Overview}
A few ScraperWiki users are responsible for a lot of Scrapers.
I guess that's not really a surprise (Pareto principle).

\begin{figure}
<<label=pareto,fig=TRUE,echo=FALSE>>=
print(figures$pareto)
@
\caption{\label{fig:pareto}Cumulative scraper ownership, where owners are ordered by decreasing number of scrapers}
\end{figure}

Among those few are most of the ScraperWiki team (figure \ref{fig:users100})
and some other users (figure \ref{fig:users100}).

\begin{table}
<<label=users100,echo=TRUE>>=
user_tables$bin100
@
\caption{\label{fig:users100}Users with at least 100 scrapers owned or edited}
\end{table}

\begin{table}
<<label=users100,echo=TRUE>>=
user_tables$bin50
@
\caption{\label{fig:users50}Users with 50--100 scrapers owned or edited}
\end{table}

\section{How does someone accumulate a lot of scrapers?}

Directly comparing two users' scraper counts is not especially useful.

\subsection{Code duplication}
Some of these high scraper counts occur because the users
have a bunch of similar scrapers (figure \ref{fig:ratio}). If we could teach
them how to iterate and paramatrize database storage,
they might write more efficient scrapers.

\begin{figure}
<<label=pareto,fig=TRUE,echo=FALSE>>=
print(figures$ratio)
@
\caption{\ref{fig:ratio}
Number of distinct word tokens in a user's scraper titles
is plotted as a function of total word tokens in the titles.
robksawyer has the most scrapers of anyone, but most of
his scrapers are especially similar (and similarly titled) to his others.
Thus, his distinct token count is comparitively low.
}
\end{figure}

What other factors affect this distinct:total token count ratio?
I considered the two users michal and AnnaPS (figure \ref{fig:ratio_low_100}).
They have similar title token counts, but AnnaPS has far more distinct tokens.
Why is this?

\begin{figure}
<<label=pareto,fig=TRUE,echo=FALSE>>=
print(figures$ratio_low_100)
@
\caption{\ref{fig:ratio_low_100}
Number of distinct word tokens in a user's scraper titles
is plotted as a function of total word tokens in the titles
for users where total word tokens is 100 or less.
What differentiates the users at the bottom of the swarm
from those at the top?
}
\end{figure}

\subsection{Branches and sandboxes}
In some cases, high scraper counts come from a lot
of sandboxes or non-functional backups.

\subsection{Splitting a scraper into multiple scripts}
A distinct:total token count ratio can also be reduced by
splitting a scraper across multiple similarly titled scripts
michal has many such branches (figure \ref{fig:michal}).

\begin{figure}
\includegraphics[width=\textwidth]{michal.png}
\caption{{fig:michal}michal splits some scrapers into several scripts}
\end{figure}

\subsection{Tutorials and forks}
AnnaPS has a lot of tutorial scrapers.

\section{How much do active ScraperWiki users know about scraping?}
For some of the users with a lot of scrapers,
I looked at what they have elsewhere on the internet.

(examples)

In general, they like freedom and openness and they
have a strong understanding of web, HTTP and XML.

\section{What scrapers do people with few scrapers have?}
To answer this, I selected a random sample of users with few scrapers.
<<label=sample,echo=TRUE>>=
set.seed(2323423)
sample(subset(users,total<3)$username,10)
@
What are those scrapers about?

Rebecca, dwf, nmajoros, donpdonp, nmajoros, srodts, rswarbrick and scraperlad each wrote one scraper.
Two of those are planning applications. (I forget which ones.)
ONILEDE had her own emailer only.
se\_test\_b709a00e\_673f\_449f had her own basic test scraper only.

<<>>=
binom.test(8,10)
@
Well that's a surprise! It looks like we can be reasonably confident
that at least half of ScraperWiki users with one or two scrapers actually
has something more than a test or a copy in that scraper.

%What did they write, why do they come, why do they leave?

\section{What is missing from this dataset}
The user list from which this dataset was constructed
was taken from a scrape of all publically browseable ScraperWiki scrapers.
As such, the dataset has no information on
  people who wrote no scrapers and
  people who didn't make accounts.
In particular, consider that data requestors
probably fall into those two groups

\section{Conclusions}
In general, I've related the number of scrapers written by ScraperWiki users
to the content of the scrapers and chracteristics of the users.

\subsection{Scrapers}\label{conclusion:scrapers}
The majority of scripts written by the more active
users should not be considered distinct scrapers.
Rather, I see them as one of the following.
\begin{itemize}
\item Repeated code from another scraper
\item Code branches/backups
\item Tutorial scraper forks
\item Splitting one scraper into multiple features
\end{itemize}
These four phenomena are less common among users with very few scripts;
it appears that a lot of people use ScraperWiki for just one scraper.

\subsection{Users}
Some of ScraperWiki's users know a \textbf{lot} about scraping.
% I think they could take on pretty much any scraping job.

%\subsection{Backups/branches}
% achieves a similar function
%to branching in a distributed version control system.

\section{Insights}
\subsection{Missing features}
It appears that power users, at least, are limited by the one-file-per scraper
setup and by the relatively weak version-control capabilities;
they are already developing work-arounds (section \ref{conclusion:scrapers}).

\subsection{Methodology}
You probably knew a lot of this already, but it might be
interesting that I set forth a way of getting
such insights from rather basic information about
the users and the scrapers.

\appendix
\section{How many scrapers per user}
<<>>=
p<-plot.total.scrapers(users.nobio)
@

\subsection{Count}
<<fig=TRUE>>=
print(p$all)
@

\subsection{On a log scale}
<<fig=TRUE>>=
print(p$all_log)
@

\subsection{Users with scrapers}
Untransformed counts, excluding counts of zero and one
<<fig=TRUE>>=
print(p$all_log)
@

\end{document}
